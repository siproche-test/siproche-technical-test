# siproche-technical-test

Bienvenue sur le test technique de Siproche.

Ce test a pour but de voir et surtout de discuter de vos compétences en programmation.

Afin de récupérer le code qui vous permettra de commencer le test, veuillez switcher sur votre branche personnelle.

Votre branche personnelle correspond à votre nom-prenom. Par exemple si vous vous appelez Silvain Degrow, votre branche sera degrow-silvain.

Une fois sur votre branch, il vous reste plus qu'à récupérer le code.

N'hésitez pas à poser vos questions si quelque chose n'est pas clair ou si vous avez un problème!

Bon test.

Team Siproche.
